# Use an official Python runtime as a parent image
FROM python:3.8.3-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install --upgrade pip
RUN pip install --trusted-host pypi.python.org -r requirements.txt
RUN pip install Flask_SQLAlchemy
RUN pip install Flask_Migrate
RUN pip install Flask_Bootstrap



# Define environment variable
ENV NAME myvenv

# Run app.py when the container launches
CMD ["python", "run.py"]
