# app/models.py

#from flask_login import UserMixin
#from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy
from app import db

class Categorie(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(60))
    sexe = db.Column(db.String(60))
    nomsexe = db.Column(db.Text)
    articles = db.relationship('Article', backref='categorie',
                                lazy='dynamic')


    def __repr__(self):
        return '<Categorie: {}>'.format(self.id)




class Article(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(60), index=True)
    description = db.Column(db.Text)
    photo = db.Column(db.Text)
    categorie_id = db.Column(db.Integer,db.ForeignKey('categories.id'))
    date_publication = db.Column(db.Date)
    stocks = db.relationship('Stock', backref='article',
                                lazy='dynamic')


    def __repr__(self):
        return '<Article: {}>'.format(self.nom)



class Stock(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'stocks'

    id = db.Column(db.Integer, primary_key=True)
    id_article = db.Column(db.Integer,db.ForeignKey('articles.id'))
    taille = db.Column(db.String(60))
    nom_couleur = db.Column(db.String(60))
    couleur = db.Column(db.String(60))
    prix_in = db.Column(db.Float)
    prix_fin = db.Column(db.Float)
    solde = db.Column(db.Integer)
    quantite = db.Column(db.Integer)
    photos = db.relationship('Photo', backref='photo',
                                lazy='dynamic')

    def __repr__(self):
        return '<Stock: {}>'.format(self.id)


class Photo(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'photos'

    id = db.Column(db.Integer, primary_key=True)
    id_stock = db.Column(db.Integer, db.ForeignKey('stocks.id'))
    lien_photo = db.Column(db.String(60))
    position = db.Column(db.Integer)


    def __repr__(self):
        return '<Photo: {}>'.format(self.id)


class Avis(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_article = db.Column(db.Integer, db.ForeignKey('articles.id'))
    note = db.Column(db.Integer)
    commentaire = db.Column(db.Text)
    id_utilisateur = db.Column(db.Integer)

    def __repr__(self):
        return '<Avis : {}'.format(self.id)
