from flask import render_template, request, Flask, flash
#from flask_login import login_required,current_user
from sqlalchemy.orm import sessionmaker

from . import home
from ..models import Article, Categorie, Stock, Photo, Avis
import json
import urllib.request
import requests
from requests import post
from .forms import ArticleForm

lien="http://127.0.0.1:5007"
accueil_articles = "http://127.0.0.1:5007"
detail_article = "http://127.0.0.1:5007/detail/"
recherche_article = "http://127.0.0.1:5007/recherche"
produits_articles = "http://127.0.0.1:5007/produits"
sexe_article = "http://127.0.0.1:5007/sexe/"
couleur_article = "http://127.0.0.1:5007/couleur/"
categorie_article = "http://127.0.0.1:5007/category/"
categorieCouleur_article = "http://127.0.0.1:5007/categorie_couleur/"
categorieSexe_article = "http://127.0.0.1:5007/categorie_sexe/"


@home.route('/')
def index():
    #articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin).limit(20)
    mes_articles = urllib.request.urlopen(accueil_articles).read()
    mes_articles = json.loads(mes_articles.decode('utf-8'))

    articles = mes_articles["articles"]
    categories = mes_articles["categories"]
    marticles = mes_articles["best-seller"]



    #categories = Categorie.query.distinct('nom').limit(4)

    #marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)

    return render_template("article/index.html",articles=articles,categories=categories,marticles=marticles)

@home.route('/detail/<int:id>')
def detail(id):
    """
    article = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.categorie_id,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).filter_by(id=id)
    for a in article:
        idcat = a.categorie_id

    categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
    marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)
    avis = Avis.query.filter_by(id_article=id)
    """

    mon_article = urllib.request.urlopen(detail_article+str(id)).read()
    mon_article = json.loads(mon_article.decode('utf-8'))

    article = mon_article['article']
    articles = mon_article['articles']
    categories = mon_article['categories']
    avis = mon_article['avis']
    marticles = mon_article['best-seller']




    return render_template("article/single-product.html",article=article,articles=articles,categories=categories,avis=avis,marticles=marticles)


@home.route('/recherche',methods=['POST'])
def recherche():
    """
    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).filter(Article.nom.like("%"+request.form.get('recherche')+"%"))
    """

    mes_articles = urllib.request.urlopen(recherche_article).read()
    mes_articles = json.loads(mes_articles.decode('utf-8'))

    articler = mon_article['articler']
    articles = mon_article['articles']
    categories = mon_article['categories']



    return render_template("article/product_search.html",articles=articles,articler=articler,categories=categories)


@home.route("/produits",methods=['POST','GET'])
def produits():
    """
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).order_by('prix_in').all()
            categories = Categorie.query.distinct('nom').all()
            nbreArticles = 0
            for article in articles.items:
                nbreArticles = nbreArticles + 1
            return render_template("article/products.html",articles=articles,categories=categories,nbarticles=nbreArticles)

        if request.form.get('tri') == 'produit':
            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).paginate(page,per_page=24)
            categories = Categorie.query.distinct('nom').all()
            nbreArticles = 0
            for article in articles.items:
                nbreArticles = nbreArticles + 1
            return render_template("article/products.html",articles=articles,categories=categories,nbarticles=nbreArticles)


        if request.form.get('tri') == 'produit':
            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).paginate(page,per_page=24)
            categories = Categorie.query.distinct('nom').all()
            nbreArticles = 0
            for article in articles:
                nbreArticles = nbreArticles + 1
            return render_template("article/products.html",articles=articles,categories=categories,nbarticles=nbreArticles)

    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).paginate(page,per_page=24)
    categories = Categorie.query.distinct('nom').all()
    nbreArticles = 0
    for article in articles.items:
        nbreArticles = nbreArticles + 1
    """

    mes_articles = urllib.request.urlopen(produits_articles).read()
    mes_articles = json.loads(mes_articles.decode('utf-8'))

    articles = mes_articles['articles']
    marticles = mes_articles['articles']
    categories = mes_articles['categories']
    nbreArticles = mes_articles['nbarticles']


    return render_template("article/products.html",articles=articles,marticles=marticles,categories=categories,nbreArticles=nbreArticles)


@home.route("/sexe/<s>")
def sexe(s):
    """
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            sexe = Categorie.query.filter_by(sexe=s)

            articlec = ""
            for s in sexe:
                articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=s.id).order_by('prix_in')

            s = s.sexe

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            nbreArticles = 0
            for article in articles:
                nbreArticles = nbreArticles + 1
            categories = Categorie.query.distinct('nom').all()

            return render_template("article/sexe.html",articlec=articlec,articles=articles,categories=categories,s=s,nbarticles=nbreArticles)

        if request.form.get('tri') == 'produit':
            sexe = Categorie.query.filter_by(sexe=s)

            articlec = ""
            for s in sexe:
                articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=s.id).order_by('nom')

            s = s.sexe

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1
            categories = Categorie.query.distinct('nom').all()

            return render_template("article/sexe.html",articlec=articlec,articles=articles,categories=categories,s=s,nbarticles=nbreArticles)

    sexe = Categorie.query.filter_by(sexe=s)

    articlec = ""
    for s in sexe:
        articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=s.id)

    s = s.sexe

    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
    nbreArticles = 0
    for article in articlec:
        nbreArticles = nbreArticles + 1
    categories = Categorie.query.distinct('nom').all()
    """

    mes_articles = urllib.request.urlopen(sexe_article+str(s)).read()
    mes_articles = json.loads(mes_articles.decode('utf-8'))

    article = mes_articles['article']
    marticles = mes_articles['articles']
    categories = mes_articles['categories']
    nbreArticles = mes_articles['nbarticles']
    sexe = mes_articles['categorie_sexe']

    return render_template("article/sexe.html",articlec=article,articles=marticles,categories=categories,s=sexe,nbarticles=nbreArticles)



@home.route("/category/<c>")
def category(c):
    """
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            cat = Categorie.query.filter_by(nom=c)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.categorie_id,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).order_by('prix_in')
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = c.nom

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            categories = Categorie.query.distinct('nom').all()

            return render_template("article/category.html",articlec=articlec,articles=articles,categories=categories,c=c,nbarticles=nbreArticles)

        if request.form.get('tri') == 'produit':
            cat = Categorie.query.filter_by(nom=c)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.categorie_id,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).order_by('nom')
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = c.nom

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            categories = Categorie.query.distinct('nom').all()

            return render_template("article/category.html",articlec=articlec,articles=articles,categories=categories,c=c,nbarticles=nbreArticles)

    cat = Categorie.query.filter_by(nom=c)
    idcategory
    for c in cat:
        idcategory=c.id

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.categorie_id,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory)
    nbreArticles = 0
    for article in articlec:
        nbreArticles = nbreArticles + 1

    c = c.nom

    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
    categories = Categorie.query.distinct('nom').all()
    """

    mes_articles = urllib.request.urlopen(categorie_article + str(c)).read()
    mes_articles = json.loads(mes_articles.decode('utf-8'))

    article = mes_articles['article']
    marticles = mes_articles['articles']
    categories = mes_articles['categories']
    nbreArticles = mes_articles['nbarticles']
    categorie = mes_articles['categorie']

    return render_template("article/category.html",articles=marticles,articlec=article,categories=categories,categorie=categorie,nbreArticles=nbreArticles)


@home.route('/couleur/<color>')
def couleur(color):
    """
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).order_by('prix_in')
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            categories = Categorie.query.distinct('nom').all()


            return render_template("article/couleur.html",articlec=articlec,articles=articles,categories=categories,c=c,nbarticles=nbreArticles)

        if request.form.get('tri') == 'produit':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).order_by('nom')
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            categories = Categorie.query.distinct('nom').all()


            return render_template("article/couleur.html",articlec=articlec,articles=articles,categories=categories,c=c,nbarticles=nbreArticles)

    nom_couleur = Stock.query.filter_by(nom_couleur=color)
    idart = 0

    for c in nom_couleur:
        idart = c.id_article

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart)
    nbreArticles = 0
    for article in articlec:
        nbreArticles = nbreArticles + 1

    c = color

    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
    categories = Categorie.query.distinct('nom').all()
    """

    mes_articles = urllib.request.urlopen(couleur_article+str(color)).read()
    mes_articles = json.loads(mes_articles.decode('utf-8'))

    article = mes_articles['article']
    marticles = mes_articles['articles']
    categories = mes_articles['categories']
    nbreArticles = mes_articles['nbarticles']
    couleur = mes_articles['couleur']

    return render_template("article/couleur.html",articles=marticles,articlec=article,categories=categories,nbreArticles=nbreArticles,couleur=couleur)


@home.route('/categorie_couleur/<cat>/<color>')
def categorie_couleur(cat,color):
    """
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            cat = Categorie.query.filter_by(nom=cat)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).filter_by(categorie_id=idcategory).order_by('prix_in')
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            categories = Categorie.query.distinct('nom').all()


            return render_template("article/categorie_couleur.html",articlec=articlec,articles=articles,categories=categories,c=c,nbarticles=nbreArticles)

        if request.form.get('tri') == 'produit':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            cat = Categorie.query.filter_by(nom=cat)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).filter_by(categorie_id=idcategory).order_by('prix_in')

            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            categories = Categorie.query.distinct('nom').all()


            return render_template("article/categorie_couleur.html",articlec=articlec,articles=articles,categories=categories,c=c,nbarticles=nbreArticles)

    nom_couleur = Stock.query.filter_by(nom_couleur=color)
    idart = 0

    for c in nom_couleur:
        idart = c.id_article

    cat = Categorie.query.filter_by(nom=cat)
    idcategory = 0
    for c in cat:
        idcategory=c.id

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).filter_by(categorie_id=idcategory).order_by('prix_in')

    nbreArticles = 0
    for article in articlec:
        nbreArticles = nbreArticles + 1

    c = color

    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
    categories = Categorie.query.distinct('nom').all()
    """

    mes_articles = urllib.request.urlopen(categorieCouleur_article+str(cat)+"/"+str(color)).read()
    mes_articles = json.loads(mes_articles.decode('utf-8'))

    article = mes_articles['article']
    marticles = mes_articles['articles']
    categories = mes_articles['categories']
    nbreArticles = mes_articles['nbarticles']
    categorie_couleur = mes_articles['categorie_couleur']


    return render_template("article/categorie_couleur.html",articles=marticles,articlec=article,categories=categories,nbreArticles=nbreArticles,categorie_couleur=categorie_couleur)


@home.route("/categorie_sexe/<cat>/<s>")
def categorie_sexe(cat,s):
    """
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            sexe = Categorie.query.filter_by(sexe=s)

            cat = Categorie.query.filter_by(nom=cat)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = ""
            for s in sexe:
                articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).filter_by(sexe=s).order_by('prix_in')

            s = s.sexe

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            nbreArticles = 0
            for article in articles:
                nbreArticles = nbreArticles + 1
            categories = Categorie.query.distinct('nom').all()

            return render_template("article/categorie_sexe.html",articlec=articlec,articles=articles,categories=categories,s=s,nbarticles=nbreArticles)

        if request.form.get('tri') == 'produit':
            sexe = Categorie.query.filter_by(sexe=s)

            cat = Categorie.query.filter_by(nom=cat)

            for c in cat:
                idcategory=c.id

            articlec = ""
            for s in sexe:
                articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).filter_by(sexe=s).order_by('prix_in')

            s = s.sexe

            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
            nbreArticles = 0
            for article in articlec:
                nbreArticles = nbreArticles + 1
            categories = Categorie.query.distinct('nom').all()

            return render_template("article/categorie_sexe.html",articlec=articlec,articles=articles,categories=categories,s=s,nbarticles=nbreArticles)

    sexe = Categorie.query.filter_by(sexe=s)

    cat = Categorie.query.filter_by(nom=cat)

    for c in cat:
        idcategory=c.id

    articlec = ""
    for s in sexe:
        articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).filter_by(sexe=s).order_by('prix_in')

    s = s.sexe

    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
    nbreArticles = 0
    for article in articlec:
        nbreArticles = nbreArticles + 1
    categories = Categorie.query.distinct('nom').all()
    """

    mes_articles = urllib.request.urlopen(categorieSexe_article+str(cat)+"/"+str(s)).read()
    mes_articles = json.loads(mes_articles.decode('utf-8'))

    article = mes_articles['article']
    marticles = mes_articles['articles']
    categories = mes_articles['categories']
    nbreArticles = mes_articles['nbarticles']
    categorie_sexe = mes_articles['categorie_sexe']

    return render_template("article/categorie_sexe.html",articles=marticles,articlec=article,categories=categories,nbreArticles=nbreArticles,categorie_sexe=categorie_sexe)






































app = Flask(__name__)

app.config['UPLOADED_IMAGES_DEST_article'] = 'app/static/img/articles'



URL_ARTICLES_vendeur=lien+'/articles'
URL_ARTICLE_AJOUT_vendeur=lien+'/articles/ajout'
URL_ARTICLE_SUP_vendeur=lien+'/articles/sup/'
URL_ARTICLE_MOD_vendeur=lien+'/articles/modif/'
URL_STOCKS_vendeur=lien+'/stocks/'
URL_STOCK_AJOUT_vendeur=lien+'/stocks/ajout/'
URL_STOCK_SUP_vendeur=lien+'/stocks/sup/'
URL_STOCK_MOD_vendeur=lien+'/stock/modif/'
URL_stock_vendeur=lien+'/stock/'
URL_article_vendeur=lien+'/article/'







@home.route('/articles', methods=['GET', 'POST'])
def liste_article():


    req_articles = urllib.request.urlopen(URL_ARTICLES_vendeur).read()
    articles = json.loads(req_articles.decode('utf-8'))
    articles = articles['articles']
    articless=[]
    for article in articles:
        art=Article(id=article['id_article'],nom=article['nom'],categorie_id=article['id_categorie'],date_publication=article['date_publication'],photo=article['photo'])
        articless.append(art)


    #return articless

    return render_template('article/articles.html',articles=articless, title="Articles")



@home.route('/articles/ajout', methods=['GET', 'POST'])
def ajoutArticle():
    form = ArticleForm()
    test="non"
    if form.validate_on_submit():



        try:

            # add department to the database




            article = Article(nom=form.name.data,categorie=form.categorie.data)

            json_article={
                "nom": article.nom,
                "id_categorie": article.categorie_id,
            }



            response = jsonify(json_article)
            r = response.json
            flash('You have successfully edited the command.')
            id = requests.post(url=URL_ARTICLE_AJOUT_vendeur, json=r)


            image=form.photo.data
            extentions=image.filename.split(".")
            extention=extentions[len(extentions)-1]
            photo_name="photo"+str(id)+"."+extention
            filename = image.save(os.path.join(app.config['UPLOADED_IMAGES_DEST_article'], photo_name))
            article.photo=photo_name



            json_article={
                "id_article": article.id,
                "nom": article.nom,
                "id_categorie": article.categorie_id,
                "photo":photo_name
            }
            response = jsonify(json_article)
            r = response.json
            flash('You have successfully edited the command.')
            r = requests.post(url=URL_ARTICLE_MOD_vendeur+str(id), json=r)
            flash('un nouvel article a été ajouté.')
        except:
            # in case department name already exists
            flash("Error: ce nom d'article exite deja ")

        # redirect to departments page
        form = ArticleForm()




        return redirect(url_for('home.liste_article'))

    # load department template
    return render_template('article/article.html', action="Ajout",form=form,title="Ajout Artcile",test=test)







@home.route('/articles/sup/<int:id>', methods=['GET', 'POST'])
def supArticle(id):


    #requests.post(url=URL_ARTICLE_SUP_vendeur+str(id))
    articles = Article.query.all()
    flash('Suppression effectué avec succès.')
    return redirect(url_for('home.liste_article'))




@home.route('/articles/modif/<int:id>', methods=['GET', 'POST'])
def modifArticle(id):



    req_article = urllib.request.urlopen(URL_article_vendeur+str(id)).read()
    article = json.loads(req_stock.decode('utf-8'))

    article = Article(id_article=article['id_article'],nom=article['nom'],id_categorie=article['id_categorie'],photo=article['photo'])


    form = ArticleForm(obj=article)
    if form.validate_on_submit():


        article.nom = form.name.data
        article.categorie = form.categorie.data


        json_article={
                "id_article": article.id,
                "nom": article.nom,
                "id_categorie": article.categorie_id,

            }

        response = jsonify(json_article)
        r = response.json
        flash('You have successfully edited the command.')
        r = requests.post(url=URL_ARTICLE_MOD_vendeur+str(id), json=r)


        flash('Changement effectué avec succès')

        # redirect to the roles page
        return redirect(url_for('home.liste_article'))

    form.categorie.data = article.categorie
    form.name.data = article.nom
    form.photo.data = article.photo

    # load department template
    return render_template('article/article.html', action="Modification",form=form,title="Modification Article")













app.config['UPLOADED_IMAGES_DEST'] = 'app/static/img/stocks'








@home.route('/stocks/ajout/<int:id>', methods=['GET', 'POST'])
def ajout_stock(id):
    form = StockForm()
    couleur="non"
    if form.validate_on_submit():


        req_article = urllib.request.urlopen(URL_article_vendeur+str(id)).read()
        article = json.loads(req_stock.decode('utf-8'))

        article = Article(id_article=article['id_article'],nom=article['nom'],id_categorie=article['id_categorie'],photo=article['photo'])
        stock = Stock(article=article,taille=form.taille.data,couleur=str(form.couleur.data),nom_couleur=str(form.nom_couleur.data),prix_in=form.prix.data,prix_fin=form.prix.data - (form.prix.data*form.solde.data)/100,solde=form.solde.data,quantite=form.quantite.data)

        db.session.add(stock)
        db.session.commit()
        json_stock={

            "id_stocks":stock.id,
            "id_article": stock.id_article,
            "prix_in":stock.prix_in,
            "prix_fin":stock.prix_fin,
            "solde":stock.solde,
            "taille":stock.taille,
            "nom_couleur":stock.nom_couleur,
            "couleur":stock.couleur,
            "quantite":stock.quantite
        }

        image=form.photo1.data
        extentions=image.filename.split(".")
        extention=extentions[len(extentions)-1]
        photo_name1="photo1"+str(stock.id)+"."+extention
        filename1 = image.save(os.path.join(app.config['UPLOADED_IMAGES_DEST'],photo_name1))
        photo=Photo(id_stock=stock.id,lien_photo=photo_name1,position=1)

        json_stock['photo']=photo_name1

        db.session.add(photo)
        db.session.commit()

        if form.photo2.data is not None:
            image=form.photo2.data
            extentions=image.filename.split(".")
            extention=extentions[len(extentions)-1]
            photo_name2="photo2"+str(stock.id)+"."+extention
            filename2 = image.save(os.path.join(app.config['UPLOADED_IMAGES_DEST'],photo_name2))
            photo=Photo(id_stock=stock.id,lien_photo=photo_name2,position=2)
            json_stock['photo2']=photo_name2



        if form.photo3.data is not None:
            image=form.photo3.data
            extentions=image.filename.split(".")
            extention=extentions[len(extentions)-1]
            photo_name3="photo3"+str(stock.id)+"."+extention
            filename3 = image.save(os.path.join(app.config['UPLOADED_IMAGES_DEST'], photo_name3))
            photo=Photo(id_stock=stock.id,lien_photo=photo_name3,position=3)
            json_stock['photo3']=photo_name3



        if form.photo4.data is not None:
            image=form.photo4.data
            extentions=image.filename.split(".")
            extention=extentions[len(extentions)-1]
            photo_name4="photo4"+str(stock.id)+"."+extention
            filename4 = image.save(os.path.join(app.config['UPLOADED_IMAGES_DEST'], photo_name4))
            photo=Photo(stock=stock,lien_photo=photo_name4,position=4)
            json_stock['photo4']=photo_name4

        response = jsonify(json_stock)
        r = response.json
        flash('You have successfully edited the command.')
        r = requests.post(url=URL_STOCK_AJOUT_vendeur+str(id), json=r)




        couleur=form.couleur.data
        """
        try:
            # add department to the database
            db.session.add(article)
            db.session.commit()
            flash('un nouvel article a été ajouté.')
        except:
            # in case department name already exists
            flash("Error: ce nom d'article exite deja ")




        return render_template('article/stock.html', action="Ajout",form=form,title="Ajout Artcile", couleur=couleur)
        """
        #return render_template('article/stock.html', action="Ajout",form=form,title="Ajout Artcile",couleur=couleur)
    # load department template
    return render_template('article/stock.html', action="Ajout",form=form,title="Ajout Artcile",couleur=couleur)



@home.route('/stocks/<int:id>', methods=['GET', 'POST'])
def list_stock(id):

    stocks = Stock.query.all()
    stocks=db.session.query(Stock.id,Stock.id_article,Stock.prix_in,Stock.prix_fin,Stock.solde,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.quantite).filter(Stock.id_article==id).all()
    return render_template('article/stocks.html',stocks=stocks, title="Articles")



@home.route('/stock/modif/<int:id>', methods=['GET', 'POST'])
def modif_stock(id):

    req_stock = urllib.request.urlopen(URL_stock_vendeur+str(id)).read()
    stock = json.loads(req_stock.decode('utf-8'))

    stock = Stock(id_article=stock['id_article'],taille=stock['taille'],couleur=stock['couleur'],nom_couleur=stock['nom_couleur'],prix_in=stock['prix_in'],prix_fin=stock['prix_fin'] ,solde=stock['solde'],quantite=stock['quantite'])


    form = StockForm(obj=stock)

    if form.validate_on_submit():

        Stock.prix_in = form.prix.data
        Stock.article=form.article.data
        Stock.prix_in=form.prix.data
        Stock.nom_couleur=form.nom_couleur.data
        Stock.prix_fin=form.prix.data - (form.prix.data*form.solde.data)/100
        Stock.solde=form.solde.data
        Stock.taille=form.taille.data
        Stock.couleur=str(form.couleur.data)
        Stock.quantite=form.quantite.data


        json_stock={

            "id_stocks":stock.id,
            "id_article": stock.id_article,
            "prix_in":stock.prix_in,
            "prix_fin":stock.prix_fin,
            "solde":stock.solde,
            "taille":stock.taille,
            "nom_couleur":stock.nom_couleur,
            "couleur":stock.couleur,
            "quantite":stock.quantite

        }
        response = jsonify(json_stock)
        r = response.json
        flash('You have successfully edited the command.')
        r = requests.post(url=URL_STOCK_MOD_vendeur+str(id), json=r)




        flash('Changement effectué avec succès')

        # redirect to the roles page
        return redirect(url_for('home.liste_article'))


    # load department template
    return render_template('article/stock.html', action="Modification",form=form,title="Modification Article",stock=stock)




@home.route('/articles/sup/<int:id>', methods=['GET', 'POST'])
def supStock(id):
    r = requests.post(url=URL_STOCK_SUP_vendeur+str(id))
    flash('Suppression effectué avec succès.')
    return redirect(url_for('home.liste_stock'))
