# app/article/forms.py

import datetime
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField,FileField,FloatField,IntegerField
from wtforms_components import ColorField
from wtforms.validators import DataRequired

from wtforms.ext.sqlalchemy.fields import QuerySelectField,QuerySelectMultipleField

#from flask_uploads import UploadSet, IMAGES
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed, FileRequired

from ..models import  Article, Stock, Categorie, Photo



class ArticleForm(FlaskForm):
    """
    Form for article to add or edit a role
    """
    name = StringField("Nom de l'article", validators=[DataRequired()])


    categorie = QuerySelectField(query_factory=lambda: Categorie.query.all(),get_pk=lambda a: a.id,blank_text=u'choisissez une categorie...',get_label="nomsexe")

    photo = FileField('image', validators=[DataRequired() ,FileAllowed(['jpg', 'png','jpeg'], 'Image uniquement!')])

    submit = SubmitField('Submit')



class StockForm(FlaskForm):

    #Form for article to add or edit a role

    article = QuerySelectField(query_factory=lambda: Article.query.all(),
                            get_label="nom")
    taille = StringField('Taille', validators=[DataRequired()])
    nom_couleur = StringField('Nom couleur', validators=[DataRequired()])
    couleur = ColorField()
    prix = FloatField('Prix', validators=[DataRequired()], default=0)
    solde =IntegerField('Solde',default=0)
    quantite = IntegerField('Quantite', validators=[DataRequired()])
    photo1 = FileField('image principal',validators=[FileAllowed(['jpg', 'png','jpeg'], 'Image uniquement!')])
    photo2 = FileField('image secondaire',validators=[FileAllowed(['jpg', 'png','jpeg'], 'Image uniquement!')])
    photo3 = FileField('image secondaire',validators=[FileAllowed(['jpg', 'png','jpeg'], 'Image uniquement!')])
    photo4 = FileField('image secondaire',validators=[FileAllowed(['jpg', 'png','jpeg'], 'Image uniquement!')])

    submit = SubmitField('Submit')
